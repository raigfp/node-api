var express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    Bear = require('./models/bear'),
    app = express(),
    port = process.env.PORT || 8080,
    router = express.Router();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose.connect('mongodb://user:user@localhost:27017/test');

router.use(function(req, res, next) {
    console.log('Something is happening.');
    next();
});

router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api' });
});

router.route('/bears')
    .post(function(req, res) {
        var bear = new Bear();
        bear.name = req.body.name;

        console.log(bear.name);

        bear.save(function(err) {
            if (err) {
                response.send(err)
            }

            res.json({ message: 'Bear created!' });
        });
    })
    .get(function(req, res) {
        Bear.find(function(err, bears) {
            if (err) {
                response.send(err);
            }

            res.json(bears);
        });
    });

app.use('/api', router);

app.listen(port);
console.log('Magic happens on port ' + port);
